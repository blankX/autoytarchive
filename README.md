# AutoYTArchive

Automatically archives YouTube videos and streams to Telegram

### Installation Instructions
1. Install:
    - `python3` (this is in python after all)
    - `ffmpeg` (to download)
2. `pip3 install -r requirements.txt`
3. Copy example-config.yaml to config.yaml and edit it
4. `python3 -m autoytarchive nodl`

### Start
`python3 -m autoytarchive`
