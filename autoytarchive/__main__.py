import sys
import json
import asyncio
import logging
from . import config, client, seen_videos
from .workers import check_channels, video_worker, upload_worker
from .utils import update_seen_videos

async def main(nodl):
    await client.start(bot_token=config['telegram']['bot_token'])
    try:
        message = await client.get_messages(config['config']['storage_chat_id'], ids=config['config']['storage_message_id'])
        resp = await message.download_media(bytes)
        seen_videos.extend(json.loads(resp))
    except BaseException:
        logging.exception('Exception encountered when downloading seen videos')
    if nodl:
        await check_channels(True)
        await update_seen_videos()
        await client.disconnect()
    else:
        await asyncio.gather(
            check_channels(False),
            *[video_worker() for _ in range(config['config'].get('video_workers', 1))],
            *[upload_worker() for _ in range(config['config'].get('upload_workers', 1))]
        )
    await client.disconnect()

if len(sys.argv) not in (1, 2):
    print('Usage:', sys.executable, '-m', __package__, '[nodl]', file=sys.stderr)
    exit(1)
if len(sys.argv) == 2 and sys.argv[1] != 'nodl':
    print('Usage:', sys.executable, '-m', __package__, '[nodl]', file=sys.stderr)
    exit(1)
client.loop.run_until_complete(main(sys.argv[-1] == 'nodl'))
