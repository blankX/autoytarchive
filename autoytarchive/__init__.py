import logging
logging.basicConfig(level=logging.INFO)

import yaml
import aiohttp
from telethon import TelegramClient

with open('config.yaml') as file:
    config = yaml.safe_load(file)

session = aiohttp.ClientSession()
client = TelegramClient('autoytarchive', config['telegram']['api_id'], config['telegram']['api_hash'])
seen_videos = []
